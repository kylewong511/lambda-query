import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.xuejike.query.core.JQuerys;
import com.github.xuejike.query.mongo.demo.App;
import com.github.xuejike.query.mongo.demo.enums.U1Status;
import com.github.xuejike.query.mongo.demo.mybatis.entity.U1;
import com.github.xuejike.query.mongo.demo.mybatis.entity.U2;
import com.github.xuejike.query.mongo.demo.vo.U1Vo;
import com.github.xuejike.query.mybatisplus.MyBatisPlusDaoFactory;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

/**
 * @author xuejike
 * @date 2020/12/31
 */
@SpringBootTest(classes = App.class)
@ContextConfiguration
@Slf4j
@Rollback(value = false)
public class MyBatisTest {

    @BeforeEach
    public void before(){
        new MyBatisPlusDaoFactory(SpringUtil.getBeansOfType(BaseMapper.class).values());

    }

    @Test
    public void list(){
        List<U1Vo> list = JQuerys.lambdaQuery(U1.class)
                .eq(U1::getStatus, U1Status.T1)
                .loadRef(U1::getU2Id, U2.class, U2::getId)
                .map(U1Vo.class).list();
        System.out.println(JSON.toJSONString(list));
    }
}
