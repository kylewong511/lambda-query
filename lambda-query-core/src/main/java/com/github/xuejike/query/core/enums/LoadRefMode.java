package com.github.xuejike.query.core.enums;

import cn.hutool.cache.impl.NoCache;

/**
 * @author xuejike
 * @date 2020/12/31
 */
public enum LoadRefMode {
    noCache,useCache
}
